//
//  MeViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage
import FirebaseStorageUI

class MeViewController: UIViewController {

    @IBOutlet weak var profileImageView: ImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var postsLabel: UILabel!
    @IBOutlet weak var sentHugsLabel: UILabel!
    @IBOutlet weak var receivedHugsLabel: UILabel!

    
    var profileService: ProfileService!
    var hugService: HugService!
    
    var posts = [Post]()
    var hugs = [Hug]()
    
    fileprivate let postSectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let hugSectionInsets = UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0)

    fileprivate let itemsPerRow: CGFloat = 3
    
    
    override func viewDidLoad() {
        super.viewDidLoad()


        profileService = ProfileService()
        hugService = HugService()
        
        self.setProfileDetails()
        
        
    }
    

    private func setProfileDetails() {
        
        let url = URL(string: User.profile.photoURL)
        self.profileImageView.sd_setImage(with: url, completed: nil)
        self.navigationItem.title = User.profile.firstName
        
        profileService.fetchPosts {
            self.posts = User.profile.posts
            
            if self.posts.count == 0 {
                self.postsLabel.text = "0"
           
            } else {
                self.postsLabel.text = "\(self.posts.count)"
                self.collectionView.reloadData()
            }
            
        }
        
        profileService.fetchSentHugs {
            self.hugs = User.profile.sentHugs
            self.sentHugsLabel.text = "\(User.profile.sentHugs.count)"
            self.collectionView.reloadData()
        }
        
        if User.profile.receivedHugs.count == 0 {
            
            self.hugService.fetchHugs {
                User.profile.receivedHugs = self.hugService.hugs
                self.receivedHugsLabel.text = "\(User.profile.receivedHugs.count)"
            }
       
        } else {
            self.receivedHugsLabel.text = "\(User.profile.receivedHugs.count)"
        }
    }
    
    @IBAction func didChangeContent(_ sender: UISegmentedControl) {
        self.collectionView.reloadData()
    }
    
    @IBAction func moreDidTap(_ sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let seeTermsAction = UIAlertAction(title: "Terms of Use", style: .default) { (action) in
            
            
        }
        
        alertController.addAction(seeTermsAction)
        
        let seePolicyAction = UIAlertAction(title: "Privacy Policy", style: .default) { (action) in
            
            
        }
        
        alertController.addAction(seePolicyAction)
        
        let signOutAction = UIAlertAction(title: "Sign out", style: .destructive) { (action) in

            print("signOutAction")
            try! Auth.auth().signOut()

            self.profileService.clearProfile()

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let signIn = storyboard.instantiateViewController(withIdentifier: "SignIn")

            self.present(signIn, animated: true, completion: nil)
        }

        alertController.addAction(signOutAction)

        let deleteAccountAction = UIAlertAction(title: "Delete account", style: .destructive) { (action) in

            let aController = UIAlertController(title: "Sure?", message: "You are about to delete your account", preferredStyle: .actionSheet)

            let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in

                self.profileService.deleteAccount {

                    self.profileService.clearProfile()

                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let signIn = storyboard.instantiateViewController(withIdentifier: "SignIn")

                    self.present(signIn, animated: true, completion: nil)
                }
            })

            aController.addAction(deleteAction)

            let cAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

            aController.addAction(cAction)

            self.present(aController, animated: true, completion: nil)
        }

        alertController.addAction(deleteAccountAction)

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
        
    }
    
   @objc func seeHugsDidTap() {
        self.performSegue(withIdentifier: "toHugs", sender: nil)
    }
    
    
}

// MARK: Table View
extension MeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if segmentControl.selectedSegmentIndex == 0 {
            return self.posts.count
        }
        
        return self.hugs.count 
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if segmentControl.selectedSegmentIndex == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Post", for: indexPath) as! MePostCell
            let post = User.profile.posts[indexPath.row]
            cell.updateUI(withPost: post)
            User.profile.selectedPost = post 
            cell.seeHugsButton.addTarget(self, action: #selector(seeHugsDidTap), for: .touchUpInside)

            return cell
            
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Hug", for: indexPath) as! MeHugCell
            
            let hug = User.profile.sentHugs[indexPath.row]
            cell.updateUI(withHug: hug)
            
            return cell
        }

    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width : CGFloat
        let height : CGFloat
        
        if segmentControl.selectedSegmentIndex == 0 {
            width = collectionView.frame.width
            height = 80
            return CGSize(width: width, height: height)
       
        } else {
            
            width = 60
            height = 90
            return CGSize(width: width, height: height)
        }
        
    }
}


