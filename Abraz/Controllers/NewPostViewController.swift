//
//  NewPostViewController.swift
//  Abraz
//
//  Created by Mehmet Tarhan on 15.03.2018.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import UIKit

class NewPostViewController: UIViewController {

    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var contentView: UIView!
    
    var postService: PostService!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        postService = PostService()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancelDidTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneDidTap(_ sender: UIButton) {
        self.contentView.backgroundColor = UIColor.darkGray
    
        let post = Post(howBad: Double(slider.value))
        self.postService.savePost(post: post)
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sliderDidChange(_ sender: UISlider) {
        self.doneButton.isEnabled = true
        self.doneButton.backgroundColor = COLOR_AQUA
        self.doneButton.setTitleColor(UIColor.white, for: .normal)
        
        if sender.value < 0.37 {
            sender.tintColor = COLOR_POST_NOT_BAD
        
        } else if sender.value < 0.67 {
            sender.tintColor = COLOR_POST_NORMAL
        
        } else {
            sender.tintColor = COLOR_POST_TOO_BAD
        }
    }
}
