//
//  HoldViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit
import FirebaseAuth

class HoldViewController: UIViewController {

    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    
    var authService: AuthService!
    var profileService: ProfileService!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authService = AuthService()
        profileService = ProfileService()
        
        
        //try! Auth.auth().signOut()
//        self.performSegue(withIdentifier: "toSignIn", sender: nil)
//
        authService.autoSignIn { (message) in

            if message == nil {
                self.performSegue(withIdentifier: "toSignIn", sender: nil)

            } else {

                self.profileService.fetchDetails {
                    self.profileService.fetchBlockedUsers {
                        self.profileService.fetchReportedPosts {
                            self.performSegue(withIdentifier: "toHome", sender: nil)
                        }
                    }
                }
            }
        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIImageView.animate(withDuration: 1.5) {
            
            self.leftImageView.frame.size.height = 48*3
            self.leftImageView.frame.size.width = 24*3
            self.leftImageView.center.x = self.view.center.x - 24*2 + (48/3)
            self.leftImageView.center.y = self.view.center.y
            
            self.rightImageView.frame.size.height = 48*3
            self.rightImageView.frame.size.width = 24*3
            self.rightImageView.center.x = self.view.center.x + 24*2 - (48/3)
            self.rightImageView.center.y = self.view.center.y + 24
        }
    }
}
