//
//  MeDetailsViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 1/14/18.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import FirebaseStorageUI

class MeDetailsViewController: UIViewController {

    @IBOutlet weak var profileImageView: ImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var imagePickerController: UIImagePickerController!
    
    var profileService: ProfileService!
    var userService: UserService!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        profileService = ProfileService()
        userService = UserService()
        
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self

        
        self.setProfileDetails()
        
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/7
        self.profileImageView.clipsToBounds = true
        
    }

    private func setProfileDetails() {
        
        let url = URL(string: User.profile.photoURL)
        self.profileImageView.sd_setImage(with: url, completed: nil)
        self.usernameLabel.text = User.profile.firstName
    }
    
    @IBAction func cancelDidTap(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveDidTap(_ sender: UIBarButtonItem) {
        
        self.profileService.saveDetails()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        tabBarController.selectedIndex = 2
        
        self.present(tabBarController, animated: true, completion: nil)

    }
    
    @IBAction func changeProfilePhoto(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Profile Photo", message: "Select a photo", preferredStyle: .actionSheet)
        
        let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePickerController.sourceType = .photoLibrary
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        })
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(photoLibraryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func changeUsername(_ sender: UIButton) {
        
        let alertController = UIAlertController(title: "Change username", message: "", preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            
            textField.placeholder = "Username"
        }
        
        let okAction = UIAlertAction(title: "Done", style: .destructive) { (action) in
            
            if let username = alertController.textFields?.first?.text {
                
                self.userService.getUsernameValidation(withUsername: username, validUsername: { (valid) in
                
                    if valid == -1 {
                        
                        let aController = UIAlertController(title: "Username is taken", message: "", preferredStyle: .alert)
                        
                        let ok = UIAlertAction(title: "OK", style: .destructive, handler: nil)
                        aController.addAction(ok)
                        
                        self.present(aController, animated: true, completion: nil)
                        
                    } else if valid == 1 {
                        
                        let ref = Database.database().reference().child("usernames").child(User.profile.firstName)
                        ref.removeValue()
                        
                        self.usernameLabel.text = username.lowercased()
                        User.profile.firstName = username.lowercased()
                        self.saveButton.isEnabled = true

                    }
                    
                })
                
            }
        }
        
        alertController.addAction(okAction)
       
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}


// MARK: Image Picker Controller
extension MeDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {

            self.profileImageView.image = image
            self.saveButton.isEnabled = true
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}
