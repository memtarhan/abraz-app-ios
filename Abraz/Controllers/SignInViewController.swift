//
//  SignInViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit
import FirebaseAuth


class SignInViewController: UIViewController {

    @IBOutlet weak var termsLabel: UILabel!
    
    
    var authService: AuthService!
    var profileService: ProfileService!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        authService = AuthService()
        profileService = ProfileService()
        
        self.setTermsLabel()

    }

    
    private func setTermsLabel() {
        
        termsLabel.text = "By continuing you agree to our\nTerms of Use and Privacy Policy"
        let text = (termsLabel.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms of Use")
        underlineAttriString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range1)
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: range2)
        termsLabel.attributedText = underlineAttriString
        
    }
    
}


extension SignInViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Feature", for: indexPath) as! FeatureCell
        
        cell.updateUI(index: indexPath.row)
        
        return cell
    }
}
