//
//  HomeViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMobileAds
import Firebase

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var locationManager: CLLocationManager!
    
    var profileService: ProfileService!
    var postService: PostService!
    
    var posts: [Post]!
    
    var banner: GADBannerView!

    override func viewDidLoad() {
        super.viewDidLoad()

        postService = PostService()
        profileService = ProfileService()
        
        posts = [Post]()
        
        self.postService.fetchPosts {
            self.posts = self.postService.posts
            self.tableView.reloadData()
        }
        
        
//        banner = GADBannerView(adSize: kGADAdSizeBanner)
//        bannerView.addSubview(banner)
//
//        banner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
//        banner.rootViewController = self
//        banner.load(GADRequest())
        
        
        
        
        
        // ------------------------------
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setCurrentPostStatus()
    }
    
    private func setCurrentPostStatus() {
        
        let id = User.profile.currentPost
        
        if id != "" {
        
            self.postService.fetchPost(withId: id, fetchedPost: { (post) in
                
                if let post = post {
                    
                    let now = Date()
                    
                    let elapsedTime = now.timeIntervalSince1970-post.timestamp
                
                    if elapsedTime > 24*60*60 {
                        
                        self.postService.deletePost()
                    }
                }
            })
        }
    }
}



// MARK: Table View
extension HomeViewController: UITableViewDelegate, UITableViewDataSource, UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        if let cell = tableView.dequeueReusableCell(withIdentifier: "Post") as? PostCell {
//            
//            let p = self.posts[indexPath.row]
//            print("indexPath: \(indexPath.row)")
//            cell.updateUI(withPost: p)
//            
//            return cell
//        }
        
        return PostCell()
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        print("prefetchRowsAt \(indexPaths)")
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        print("cancelPrefetchingForRowsAt \(indexPaths)")
    }
}
