//
//  TermsViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 1/20/18.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import UIKit
import WebKit

class TermsViewController: UIViewController {

    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var webKitView: WKWebView!
    
    var status = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var urlString = ""
        
        if status == 0 {
            self.navigationBar.topItem?.title = "Terms of Use"
            
            urlString = "https://www.simansolutions.com/huggy-terms-of-use"
        
        } else if status == 1 {
            self.navigationBar.topItem?.title = "Privacy Policy"
            
            urlString = "https://www.simansolutions.com/huggy-privacy-policy"
       
        } else {
            self.navigationBar.topItem?.title = "Try again"
        }
        
        let url = URL(string: urlString)!
        let urlRequest = URLRequest(url: url)
        
        webKitView.load(urlRequest)
    }
    
    @IBAction func cancelDidTap(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}
