//
//  TabBarViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 1/17/18.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        UIApplication.shared.keyWindow?.rootViewController = self 
    }
}
