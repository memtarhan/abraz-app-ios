//
//  MePostDetailsViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 1/17/18.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import UIKit

class MePostDetailsViewController: UIViewController {

    @IBOutlet weak var hugsLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var postService: PostService!
    
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let itemsPerRow: CGFloat = 3
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.hugsLabel.text = "\(User.profile.selectedPost.numberOfHugs)"

        postService = PostService()
        
        postService.fetchHugsOf(post: User.profile.selectedPost) {
            self.collectionView.reloadData()
        }
        
    }

    @IBAction func cancelDidTap(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension MePostDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.postService.hugs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Hug", for: indexPath) as? HugCell {
            
            let hug = self.postService.hugs[indexPath.row]
            
            print("MePostDetailsViewController: \(hug.from)")
            cell.updateUI(withHug: hug)
            
            return cell
        }
        
        return HugCell()

    }
    
}
