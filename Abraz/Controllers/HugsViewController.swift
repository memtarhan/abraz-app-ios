//
//  HugsViewController.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit
import UserNotifications

class HugsViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var postService: PostService!
    var hugService: HugService!
    
    var hugs: [Hug]!
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let itemsPerRow: CGFloat = 3

    override func viewDidLoad() {
        super.viewDidLoad()

        
        postService = PostService()
        hugService = HugService()
        
        hugs = [Hug]()
        
        self.hugService.fetchHugs {
            self.hugs.removeAll()
            self.hugs.append(contentsOf: self.hugService.hugs)
            User.profile.receivedHugs = self.hugService.hugs
            self.collectionView.reloadData()
        }
        
        
//        self.hugService.fetchLastHug { (hug) in
//            
//            if let hug = hug {
//            
//                let contains = self.hugs.contains(where: { (h) -> Bool in
//                    hug.id == h.id
//                })
//                
//                if !contains {
//                    print("Not contained")
//                    self.hugs.append(hug)
//                    self.sendHugNotification(withUser: hug.from)
//                    self.collectionView.reloadData()
//
//                }
//            }
//        }
        
        
    }
    
    
    
    
    @IBAction func newPostDidTap(_ sender: UIBarButtonItem) {
        
        let alertController = UIAlertController(title: "\n\n\n", message: "", preferredStyle: .actionSheet)
        
        let margin:CGFloat = 10.0
        let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: 120)
        let customView = UIView(frame: rect)
        
        let rect1 = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: 20)
        let label1 = UILabel(frame: rect1)
        label1.text = "Need a hug?"
        label1.textAlignment = .center
        label1.font = UIFont.systemFont(ofSize: 19, weight: .bold)
        customView.addSubview(label1)
        
        let rect2 = CGRect(x: margin, y: margin+24, width: alertController.view.bounds.size.width - margin * 4.0, height: 20)
        let label2 = UILabel(frame: rect2)
        label2.text = "How bad is it?"
        label2.textAlignment = .center
        label2.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        customView.addSubview(label2)
        
        let rect3 = CGRect(x: margin, y: margin+48, width: alertController.view.bounds.size.width - margin * 4.0, height: 20)
        let slider = UISlider(frame: rect3)
        slider.minimumValue = 0
        slider.maximumValue = 1
        slider.value = 0.5
        customView.addSubview(slider)
        
        
        alertController.view.addSubview(customView)
        
        let requestAction = UIAlertAction(title: "Request", style: .destructive) { (action) in
            
            let post = Post(howBad: Double(slider.value))
            self.postService.savePost(post: post)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(requestAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    private func sendHugNotification(withUser user: String) {
        
        print("sendHugNotification")
        let content = UNMutableNotificationContent()
        content.title = "You got a hug"
        content.body = "from this \(user)"
        content.sound = UNNotificationSound.default()
        
        let identifier = "UYLLocalNotification"
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 8, repeats: false)
        
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print("Error notifications: \(error.localizedDescription)")
                
            } else {
                print("Error notifications: No Error")
                
            }
        })
        
    }
}

extension HugsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.hugs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Hug", for: indexPath) as? HugCell {
            
            let hug = self.hugs[self.hugs.count - indexPath.row - 1]
            cell.updateUI(withHug: hug)
            
            return cell
        }
        
        return HugCell()
    }
//
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//        //2
//        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
//        let availableWidth = view.frame.width - paddingSpace
//        let widthPerItem = availableWidth / itemsPerRow
//
//        return CGSize(width: widthPerItem, height: widthPerItem)
//    }
//
//    //3
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        insetForSectionAt section: Int) -> UIEdgeInsets {
//        return sectionInsets
//    }
//
//    // 4
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return sectionInsets.left
//    }
}

