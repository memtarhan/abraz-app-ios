//
//  Closures.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation


typealias Completed = () -> ()
typealias FetchedPost = (_ post: Post?) -> Void
typealias FetchedPosts = (_ posts: [Post]?) -> Void
typealias FetchedHug = (_ hug: Hug?) -> Void
typealias FetchedUser = (_ user: User?) -> Void
typealias AutoSignIn = (_ message: String?) -> Void
typealias SignInMessage = (_ message: String?) -> Void 
typealias ProfileHugged = (_ hugged: Bool) -> Void
typealias ValidUsername = (_ message: Int?) -> Void


