//
//  Colors.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/13/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import UIKit

public let COLOR_POST_NOT_BAD = UIColor(red: 255/255, green: 235/255, blue: 59/255, alpha: 1)
public let COLOR_POST_NORMAL = UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: 1)
public let COLOR_POST_TOO_BAD = UIColor(red: 244/255, green: 67/255, blue: 54/255, alpha: 1)
public let COLOR_TUNGSTEN = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1)
public let COLOR_AQUA = UIColor(red: 0/255, green: 150/255, blue: 255/255, alpha: 1)


public let COLOR_AORANGE = UIColor(red: 253/255, green: 110/255, blue: 63/255, alpha: 1)
public let COLOR_APURPLE = UIColor(red: 147/255, green: 138/255, blue: 254/255, alpha: 1)
public let COLOR_AGREEN = UIColor(red: 186/255, green: 205/255, blue: 80/255, alpha: 1)
public let COLOR_ABLUE = UIColor(red: 30/255, green: 231/255, blue: 243/255, alpha: 1)



