//
//  References.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 9.03.2018.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage

let REF_USERS = Database.database().reference().child("users")
