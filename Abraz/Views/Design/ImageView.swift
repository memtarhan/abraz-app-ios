//
//  ImageView.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit

class ImageView: UIImageView {

    override func awakeFromNib() {
        
        layer.cornerRadius = self.frame.height/2
        self.clipsToBounds = true 
    }

}
