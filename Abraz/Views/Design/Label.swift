//
//  Label.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/20/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit

class Label: UILabel {

    override func awakeFromNib() {
        
        layer.cornerRadius = self.frame.height/8
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0.2
        
    }

}
