//
//  Button.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit

class Button: UIButton {

    override func awakeFromNib() {
        
        layer.cornerRadius = self.frame.height/8
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 0.2 
        
    }

}
