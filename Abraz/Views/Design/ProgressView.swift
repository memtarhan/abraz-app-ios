//
//  ProgressView.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/13/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit

class ProgressView: UIProgressView {

    override func awakeFromNib() {
        
        layer.cornerRadius = 3
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 0.2
        
    }
}
