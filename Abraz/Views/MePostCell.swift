//
//  MePostCell.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/21/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit

class MePostCell: UICollectionViewCell {

    @IBOutlet weak var howBadView: UIProgressView!
    @IBOutlet weak var hugsLabel: UILabel!
    @IBOutlet weak var seeHugsButton: UIButton!
    
    var post: Post!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        seeHugsButton.addTarget(self, action: #selector(seeHugsDidTap), for: .touchUpInside)
    }

    func updateUI(withPost post: Post) {
        self.post = post
        
        self.hugsLabel.text = "\(post.numberOfHugs)"
        self.howBadView.progress = Float(post.howBad)
        
        let value = self.howBadView.progress
        
        if value < 0.37 {
            howBadView.progressTintColor = COLOR_POST_NOT_BAD
            
        } else if value < 0.67 {
            howBadView.progressTintColor = COLOR_POST_NORMAL
            
        } else {
            howBadView.progressTintColor = COLOR_POST_TOO_BAD
        }
    }
    
    @objc func seeHugsDidTap() {
        
        User.profile.selectedPost = self.post 
        
        
    }
}
