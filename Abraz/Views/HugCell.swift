
//
//  HugCell.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit
import FirebaseStorageUI

class HugCell: UICollectionViewCell {

    @IBOutlet weak var profileImageView: ImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    
    var userService: UserService!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userService = UserService()
        
    }
    
    func updateUI(withHug hug: Hug) {
        
        self.userService.fetchUser(withId: hug.from) { (user) in
            
            if let user = user {
                
                self.usernameLabel.text = user.firstName
                self.profileImageView.sd_setImage(with: URL(string: user.photoURL), completed: nil)
            }
        }
    }
}
