
//
//  HugPostCell.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorageUI

class PostCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var hugButton: UIButton!
    @IBOutlet weak var hugsLabel: UILabel!
    @IBOutlet weak var howBadView: UIProgressView!
    @IBOutlet weak var reportButton: UIButton!
    
    var post: Post!
    var user: User!
    
    var userService: UserService!
    var hugService: HugService!
    var postService: PostService!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userService = UserService()
        hugService = HugService()
        postService = PostService()
        
        hugButton.addTarget(self, action: #selector(hugDidTap), for: .touchUpInside)
        reportButton.addTarget(self, action: #selector(reportDidTap), for: .touchUpInside)

        
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height/7
        self.profileImageView.clipsToBounds = true
        
        

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    func updateUI(withPost post: Post) {
        self.post = post
        
        self.setHugButton(isHugged: post.profileHugged)
        
        self.hugsLabel.text = "\(post.numberOfHugs)"
        self.howBadView.progress = Float(post.howBad)
        
        let value = self.howBadView.progress
        
        if value < 0.37 {
            howBadView.progressTintColor = COLOR_POST_NOT_BAD
            
        } else if value < 0.67 {
            howBadView.progressTintColor = COLOR_POST_NORMAL
            
        } else {
            howBadView.progressTintColor = COLOR_POST_TOO_BAD
        }
        
        userService.fetchUser(withId: post.author) { (user) in
            
            if let user = user {
                self.user = user
                
                self.usernameLabel.text = user.firstName
                self.profileImageView.sd_setImage(with: URL(string: user.photoURL), completed: nil)
                
            }
        }
    }
    
    @objc func hugDidTap() {
        
        let hug = Hug(post: self.post.id, to: self.user.id)
        hugService.saveHug(hug: hug)
        
        self.setHugButton(isHugged: true)
    }
    
    @objc func reportDidTap() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    
        
        let blockAction = UIAlertAction(title: "Block", style: .destructive) { (action) in
            
            print("Action: @Block")
            
            self.userService.blockUser(userId: self.user.id)
            
            self.contentView.alpha = 0.5
            self.hugButton.isEnabled = false
        }
        
        alertController.addAction(blockAction)
        
        let reportAction = UIAlertAction(title: "Report", style: .destructive) { (action) in
            
            let aController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let spamAction = UIAlertAction(title: "It's spam", style: .destructive, handler: { (action) in
                
                self.postService.reportPost(postId: self.post.id, reportType: .Spam)
                
                self.contentView.alpha = 0.5
                self.hugButton.isEnabled = false
            })
            
            aController.addAction(spamAction)
            
            let inappropriateAction = UIAlertAction(title: "It's inappropriate", style: .destructive, handler: { (action) in
                
                self.postService.reportPost(postId: self.post.id, reportType: .Inappropriate)
                
                self.contentView.alpha = 0.5
                self.hugButton.isEnabled = false
            })
            
            aController.addAction(inappropriateAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            aController.addAction(cancelAction)
            
            UIApplication.shared.keyWindow?.rootViewController?.present(aController, animated: true, completion: nil)
        }
        
        alertController.addAction(reportAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
   
        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    private func setHugButton(isHugged hugged: Bool) {
        
        if hugged {
            self.hugButton.backgroundColor = COLOR_TUNGSTEN
            self.hugButton.setTitleColor(UIColor.white, for: .normal)
            self.hugButton.setTitle("Hugged", for: .normal)
            self.hugButton.isEnabled = false
            
        } else {
            hugButton.backgroundColor = UIColor.white
            hugButton.setTitleColor(COLOR_TUNGSTEN, for: .normal)
            hugButton.setTitle("Hug", for: .normal)
            hugButton.isEnabled = true
        }
    }
}
