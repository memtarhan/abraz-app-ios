//
//  FeatureCell.swift
//  Abraz
//
//  Created by Mehmet Tarhan on 14.03.2018.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import UIKit

class FeatureCell: UICollectionViewCell {
    
    @IBOutlet weak var bView: UIView!
    @IBOutlet weak var bLabel: UILabel!
    
    func updateUI(index: Int) {
        
        switch index {
        
        case 0:
            bView.backgroundColor = COLOR_AORANGE
            
        case 1:
            bView.backgroundColor = COLOR_APURPLE
            
        case 2:
            bView.backgroundColor = COLOR_AGREEN
            
        case 3: bView.backgroundColor = COLOR_ABLUE
        default:
            break
        }
    }
}
