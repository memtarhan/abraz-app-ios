//
//  Hug.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import FirebaseDatabase


class Hug {
    
    public var id = String()
    public var to = String()
    public var from = String()
    public var post = String()
    public var timestamp = Double()
    
    init(post: String, to: String) {
        
        self.id = Database.database().reference().child("hugs").childByAutoId().key
        self.to = to
        self.from = User.profile.id
        self.post = post
        self.timestamp = Date().timeIntervalSince1970
    }
    
    init(withData data: [String: Any]) {
        
        id = data["id"] as! String
        to = data["to"] as! String
        from = data["from"] as! String
        post = data["post"] as! String
        timestamp = data["timestamp"] as! Double
    }
    
    func toData() -> [String: Any] {
        
        let data: [String: Any] =
            ["id": id,
             "to": to,
             "from": from,
             "post": post,
             "timestamp": timestamp]
        
        return data 
    }
}
