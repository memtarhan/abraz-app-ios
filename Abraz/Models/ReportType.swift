//
//  ReportType.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 22.02.2018.
//  Copyright © 2018 Mehmet Tarhan. All rights reserved.
//

import Foundation


enum ReportType: String {
    
    case Spam = "spam"
    case Inappropriate = "inappropriate"
}
