//
//  Post.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Post {
    
    public var id = String()
    public var author = String()
    public var timestamp = Double()
    public var howBad = Double()
    public var profileHugged = Bool()
    public var numberOfHugs = Int()
    
    
    init(withId id: String) {
        self.id = id
    }
    
    init(withData data: [String: Any]) {
    
        id = data["id"] as! String
        author = data["author"] as! String
        timestamp = data["timestamp"] as! Double
        howBad = data["howBad"] as! Double
        
        if let hugs = data["hugs"] as? [String: Any] {
            numberOfHugs = hugs.count
        }
    }
    
    init(howBad value: Double) {
        
        id = Database.database().reference().child("posts").childByAutoId().key
        author = User.profile.id
        timestamp = Date().timeIntervalSince1970
        howBad = value
    }
    
    func toData() -> [String: Any] {
        
        let data: [String: Any] =
            ["id": id,
             "author": author,
             "timestamp": timestamp,
             "howBad": howBad]
        
        return data 
    }
}
