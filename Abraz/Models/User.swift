//
//  User.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import CoreLocation


class User {
    
    public static var profile = User()
    
    public var id = String()
    public var firstName = String()
    public var lastName = String()
    public var photoURL = String()
    public var email = String()
    public var currentPost = String()
    
    public var posts = [Post]()
    public var receivedHugs = [Hug]()
    public var sentHugs = [Hug]()
    public var blockedUsers = [String]()
    public var reportedPosts = [String]() 
    
    public var selectedPost: Post! 
    
    private init() {
    }
    
    init(withData data: [String: Any]) {
    
        id = data["id"] as! String
        firstName = data["firstName"] as! String
        lastName = data["lastName"] as! String
        photoURL = data["photoURL"] as! String
        
        if let p = data["currentPost"] as? String {
            currentPost = p
        }
    }
    
    func toData() -> [String: Any] {
        
        let data = ["id": id,
                    "firstName": firstName,
                    "lastName": lastName,
                    "photoURL": photoURL,
                    "currentPost": currentPost]
        
        return data 
    }
}
