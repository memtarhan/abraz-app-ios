//
//  UserService.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase


class UserService {
    
    private var _REF_USERS = Database.database().reference().child("users")
    private var _REF_USERNAMES = Database.database().reference().child("usernames")

    
    func fetchUser(withId id: String, fetchedUser: @escaping FetchedUser) {
        
        let ref = _REF_USERS.child(id).child("details")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                fetchedUser(User(withData: data))
            }
        }
    }
    
    func getUsernameValidation(withUsername username: String, validUsername: @escaping ValidUsername) {
        
        if username.rangeOfCharacter(from: [".", "#"]) != nil {
            validUsername(2)
        
        } else {
            
            let ref = _REF_USERNAMES.child(username)
            
            ref.observeSingleEvent(of: .value) { (snapshot) in
                
                if snapshot.exists() {
                    validUsername(-1)
                    
                } else {
                    validUsername(1)
                }
            }
        }
        
    }
    
    func blockUser(userId id: String) {
        
        var ref = _REF_USERS.child("\(User.profile.id)/blockedUsers")
        ref.child(id).setValue(1)
        
        ref = _REF_USERS.child("\(id)/blockedBy")
        ref.child(User.profile.id).setValue(1)
        
        Analytics.logEvent("block_user", parameters: [
            "id": User.profile.id as NSObject
            ])
    }
}
