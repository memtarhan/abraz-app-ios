//
//  HugService.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import UserNotifications
import Firebase
import FirebaseDatabase


class HugService {
    
    private var _REF_USERS = Database.database().reference().child("users")
    private var _REF_HUGS = Database.database().reference().child("hugs")
    private var _REF_HUGPOSTS = Database.database().reference().child("posts")

    var hugs = [Hug]()
    
    func saveHug(hug: Hug) {
        
        var ref = _REF_HUGS.child(hug.id)
        ref.setValue(hug.toData())
        
        ref = _REF_HUGPOSTS.child("all").child(hug.post).child("hugs").child(hug.id)
        ref.setValue(1)
        
        ref = _REF_USERS.child(hug.to).child("hugs/from").child(hug.id)
        ref.setValue(hug.post)
        
        ref = _REF_USERS.child(hug.from).child("hugs/to").child(hug.id)
        ref.setValue(hug.post)
        
        Analytics.logEvent("send_hug", parameters: [
            "id": User.profile.id as NSObject,
            "timestamp": hug.timestamp as NSObject
            ])
    }

    
    func fetchHugs(completed: @escaping Completed) {
        
        let ref = _REF_USERS.child(User.profile.id).child("hugs/from")
        
        ref.observe(.childAdded) { (snapshot) in
            
            let id = snapshot.key
            
            self.fetchHug(withId: id, fetchedHug: { (hug) in
                
                if let hug = hug {
                    
                    self.hugs.append(hug)
                    
                    completed()
                }
            })
        }
    }
    
    func fetchLastHug(fetchedHug: @escaping FetchedHug) {
        
        let ref = _REF_USERS.child(User.profile.id).child("hugs/from")

        ref.queryLimited(toLast: 1).observe(.childAdded) { (snapshot) in
            
            print("observe(.childAdded)")
            let id = snapshot.key
            
            print("Hug childAdded: \(id)")
            self.fetchHug(withId: id, fetchedHug: { (hug) in
                
                if let hug = hug {
                    fetchedHug(hug)
                }
            })
        }
    }
    
    
    func fetchHug(withId id: String, fetchedHug: @escaping FetchedHug) {
        
        let ref = _REF_HUGS.child(id)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                fetchedHug(Hug(withData: data))
            
            } else {
                fetchedHug(nil)
            }
        }
    }
    
}
