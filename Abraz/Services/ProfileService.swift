//
//  ProfileService.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage


class ProfileService {
    
    private let _REF_USERS = Database.database().reference().child("users")
    private let _REF_USERNAMES = Database.database().reference().child("usernames")
    private let _REF_POSTS = Database.database().reference().child("posts")
    private let _REF_PROFILE_IMAGES = Storage.storage().reference().child("profileImages")


    private let _postService = PostService()
    private let _hugService = HugService()
    
    
    // MARK: Saving profile data
    func saveDetails() {
        
        let ref = _REF_USERS.child(User.profile.id).child("details")
        ref.setValue(User.profile.toData())
        
    }
    
    func clearProfile() {
        
        User.profile.id = ""
        User.profile.photoURL = ""
        User.profile.firstName = ""
        User.profile.posts.removeAll()
        User.profile.sentHugs.removeAll()
        User.profile.receivedHugs.removeAll()
        User.profile.selectedPost = nil
    }
    
     func fetchDetails(completed: @escaping Completed) {

        let ref = _REF_USERS.child(User.profile.id).child("details")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in

            if let data = snapshot.value as? [String: Any] {
                User.profile = User(withData: data)
                completed()
            
            }
            
            completed()
        }
    }
    
    func fetchPosts(completed: @escaping Completed) {
        
        let ref = _REF_USERS.child(User.profile.id).child("posts")
        
        ref.observe(.value) { (snapshot) in

            if let data = snapshot.value as? [String: Any] {
                User.profile.posts.removeAll()
                
                let ids = data.keys.reversed()
                
                for id in ids {
                    
                    print("fetchPosts: \(id)")
                    self._postService.fetchPost(withId: id, fetchedPost: { (post) in
                        
                        if let post = post {
                            
                            if let index = User.profile.posts.index(where: { (p) -> Bool in
                                p.id == post.id
                                
                            }) {
                                User.profile.posts[index] = post
                                
                            } else {
                                User.profile.posts.append(post)
                            }
                            
                            completed()
                        }
                    })
                }
            }
        }
        
        completed()
    }

    func fetchSentHugs(completed: @escaping Completed) {
        
        let ref = _REF_USERS.child(User.profile.id).child("hugs/to")
        
        ref.observe(.value) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                User.profile.sentHugs.removeAll()
                
                let ids = data.keys.reversed()
                
                for id in ids {
                    
                    self._hugService.fetchHug(withId: id, fetchedHug: { (hug) in
                        
                        if let hug = hug {
                            User.profile.sentHugs.append(hug)
                            
                            completed()
                        }
                    })
                }
                
            } else {
                completed()
            }
        }
    }

    
    func fetchBlockedUsers(completed: @escaping Completed) {
        
        let ref = _REF_USERS.child(User.profile.id).child("blockedUsers")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                
                for (id, _) in data {
                    User.profile.blockedUsers.append(id)
                }
                
                completed()
            }
            
            completed()
        }
    }
    
    func fetchReportedPosts(completed: @escaping Completed) {
        
        let ref = _REF_USERS.child(User.profile.id).child("reportedPosts")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                
                for (id, _) in data {
                    User.profile.reportedPosts.append(id)
                }
                
                completed()
            }
            
            completed()
        }
    }
    
    func deleteAccount(completed: @escaping Completed) {
        
        var ref = _REF_USERS.child(User.profile.id)
        ref.removeValue()
        
        if User.profile.currentPost != "" {
            ref = _REF_POSTS.child("live").child(User.profile.currentPost)
            ref.removeValue()
        }
    
        
        let refS = _REF_PROFILE_IMAGES.child("\(User.profile.id).jpg")
        refS.delete { (error) in
            
            if error == nil {
                Auth.auth().currentUser?.delete(completion: { (error) in
                    
                    if error == nil {
                        completed()
                    
                    } else {

                    }
                })
            
            } else {
            }
        }
        
    }
}
