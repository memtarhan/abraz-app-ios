//
//  HugPostService.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/2/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase


class PostService {
    
    private let _REF_USERS = Database.database().reference().child("users")
    private let _REF_POSTS = Database.database().reference().child("posts")

    private let _hugService = HugService()
    
    var posts = [Post]()
    var hugs = [Hug]()
    
    func savePost(post: Post) {
        self.deletePost()
        
        User.profile.currentPost = post.id
        
        var ref = self._REF_POSTS.child("all").child(post.id)
        ref.setValue(post.toData())
        
        ref = self._REF_POSTS.child("live").child(post.id)
        ref.setValue(1)
        
        ref = self._REF_USERS.child(User.profile.id).child("posts").child(post.id)
        ref.setValue(1)
        
        ref = self._REF_USERS.child(User.profile.id).child("details").child("currentPost")
        ref.setValue(post.id)
        
        Analytics.logEvent("share_post", parameters: [
            "id": User.profile.id as NSObject,
            "timestamp": post.timestamp as NSObject
            ])
        
        
    }
    
    func deletePost() {
        
        let id = User.profile.currentPost
        
        if id != "" {
            let ref = _REF_POSTS.child("live").child(id)
            ref.removeValue()
            User.profile.currentPost = ""
        }
    }
    
    
    func fetchPosts(completed: @escaping Completed) {
        
        let ref = self._REF_POSTS.child("live").queryLimited(toLast: 100)
        
        ref.observe(.value) { (snapshot) in
            
            print("fetchPosts-.value")
            if let data = snapshot.value as? [String: Any] {
                
                for (id, _) in data {
                    
                    if id != User.profile.currentPost && !User.profile.reportedPosts.contains(id) {
                        
                        self.fetchPost(withId: id, fetchedPost: { (post) in
                            
                            print("fetchPost-.value")
                            if let post = post,
                                !User.profile.blockedUsers.contains(post.author) {
                                
                                self.profileHugged(post: post, hugged: { (hugged) in
                                    
                                    if let index = self.posts.index(where: { (p) -> Bool in
                                        p.id == post.id
                                        
                                    }) {
                                        post.profileHugged = hugged
                                        self.posts[index] = post
                                        
                                    } else {
                                        
                                        post.profileHugged = hugged
                                        self.posts.append(post)
                                    }
                                    
                                    completed()
                                })
                            }
                        })
                    }
                }
            }
            
            ref.observe(.childRemoved, with: { (snapshot) in
                print("fetchPosts-.childRemoved")

                let id = snapshot.key
                
                print("fetchRemovedPost-.childRemoved")

                if let index = self.posts.index(where: { (post) -> Bool in
                    post.id == id
               
                }) {
                    self.posts.remove(at: index)
                }
                
                completed()
            })
        }
    }
    
    func fetchPost(withId id: String, fetchedPost: @escaping FetchedPost) {
        
        let ref = _REF_POSTS.child("all").child(id)
        
        ref.observe(.value) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                fetchedPost(Post(withData: data))
            
            } else {
                fetchedPost(nil)
            }
        }
    }
    
    func profileHugged(post: Post, hugged: @escaping ProfileHugged) {
        
        let ref = _REF_USERS.child(User.profile.id).child("hugs/to")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let data = snapshot.value as? [String: Any] {
                
                let contains = data.values.contains(where: { (id) -> Bool in
                    id as! String == post.id
                })
                
                hugged(contains)
            
            } else {
                hugged(false)
            }
        }
    }
    
    func fetchHugsOf(post: Post, completed: @escaping Completed) {
        
        
        let ref = _REF_POSTS.child("all").child(post.id).child("hugs")
        
//        ref.observeSingleEvent(of: .value) { (snapshot) in
//
//            if let data = snapshot.value as? [String: Any] {
//
//                for (id, _) in data {
//
//                    self._hugService.fetchHug(withId: id, fetchedHug: { (hug) in
//
//                        if let hug = hug {
//                            self.hugs.insert(hug, at: 0)
//
//                            completed()
//                        }
//
//                        completed()
//
//                    })
//                }
//            }
//        }
        
        ref.observe(.childAdded) { (snapshot) in
            
            let id = snapshot.key
            
            self._hugService.fetchHug(withId: id, fetchedHug: { (hug) in
                
                if let hug = hug {
                    self.hugs.append(hug)
                    
                    completed()
                }
                
                completed()
                
            })
        }
    }
    
    func reportPost(postId id: String, reportType: ReportType) {
        
        var ref = _REF_POSTS.child("all").child(id).child("reports")
        
        ref.child(reportType.rawValue).child(User.profile.id).setValue(1)
        
        Analytics.logEvent("report_post_\(reportType.rawValue)", parameters: [
            "reporter_id": User.profile.id as NSObject,
            "post_id": id as NSObject
            ])
        
        ref = _REF_USERS.child(User.profile.id).child("reportedPosts")
        
        ref.child(id).setValue(reportType.rawValue)
    }
}
