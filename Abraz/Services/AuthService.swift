//
//  AuthService.swift
//  Huggy
//
//  Created by Mehmet Tarhan on 12/1/17.
//  Copyright © 2017 Mehmet Tarhan. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseStorage


class AuthService {
    
    private var _profileService: ProfileService! {
        return ProfileService()
    }
    
    func signIn(withCredential credential: AuthCredential, completed: @escaping Completed) {
        
    }
    
    func autoSignIn(autoSignIn: @escaping AutoSignIn) {
        
        Auth.auth().addStateDidChangeListener { (auth, user) in
            
            if let user = user {
                User.profile.id = user.uid
                
                autoSignIn("OK")
            
            } else {
                autoSignIn(nil)
            }
        }
    }
    
    
}
